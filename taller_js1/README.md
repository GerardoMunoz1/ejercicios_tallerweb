# Taller - Desarrollo en JavaScript

En este repositorio se encuentran los archivos correspondientes al desarrollo de la guía de ejercicios de JavaScript. Consiste en utilizar JavaScript para el desarrollo de:
- Calculadora.
- Adivinanzas (encontrar el número).
- Lotería (con 6 números).
- To-Do List (lista de cosas por hacer).
- Verificador de último dígito RUN.

## Recursos utilizados
- HTML5.
- JavaScript.
- CSS.
- Visual Studio Code.
- Navegador Brave.
- Bootstrap 5.3.0

## Autor
- Gerardo Muñoz.
### Ingeniería Civil en Bioinformática - Taller de Programación Web - Universidad de Talca
